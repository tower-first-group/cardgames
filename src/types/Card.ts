export type Card = {
    code: string, 
    image: string, 
    value: number | string, 
    suit: string,
}
import type { Card } from "./Card";

export type Pair = {
    turn: Card;
    beat?: Card;
}
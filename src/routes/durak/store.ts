import { get, type Writable, writable } from "svelte/store";
import { fetchApi } from "../../helpers/fetchApi";
import type { Board } from "../../types/Board";
import type { Card } from "../../types/Card";
import type { Hand } from "../../types/Hand";

export function createStore() {
    const firstPlayerHand = writable<Hand>();
    const secondPlayerHand = writable<Hand>();
    const hands = {
        first: firstPlayerHand,
        second: secondPlayerHand
    }
    const currentPlayer = writable<'first' | 'second' | 'none'>('first');
    const board = writable<Board>();
    const deck = writable()

    function makeTurn(card: Card) {
        board.set([...get(board), {turn: card}])
        console.log(get(currentPlayer))
        const currentPlayerHand = hands[get(currentPlayer)] as Writable<Card[]>;
        console.log(currentPlayerHand)

        currentPlayerHand.set(get(currentPlayerHand).filter((elem) => elem !== card));
    }

    async function init() {
        await createDeck();
        firstPlayerHand.set(await drawHand())
        secondPlayerHand.set(await drawHand())
        board.set([])
    }

    async function createDeck() {
        const deckResponse = await fetchApi("https://deckofcardsapi.com/api/deck/new/shuffle/?deck_count=1")
        deck.set(deckResponse.deck_id)
    }

    async function drawHand() {
        const handResponse = await fetchApi(`https://deckofcardsapi.com/api/deck/${get(deck)}/draw/?count=6`)
        return handResponse.cards;
    }

    return {
        firstPlayerHand,
        secondPlayerHand,
        currentPlayer,
        board,
        init,
        makeTurn
    }
}
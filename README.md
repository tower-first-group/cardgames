## Подтянуть себе гит-репозиторий

Чтобы у вас локально появился код из этого репозитория необходимо в терминале выполнить
```bash
# Клонирование репозитория
git clone https://gitlab.com/tower-first-group/cardgames.git

# Переместиться в папку проекта
сd cardgames
```

## Установка зависимостей и запуск сервера

Далее, чтобы у вас всё заработало, нужно подтянуть зависимости из npm и поднять локальный сервер

```bash
# установка зависимостей
npm i

# запуск сервера
npm run dev 
```

Вы восхитительны! Теперь в терминале появилась информация вида 
```bash
> cardgames@0.0.1 dev
> vite dev



  VITE v4.1.2  ready in 460 ms

  ➜  Local:   http://localhost:5173/
  ➜  Network: use --host to expose
  ➜  press h to show help

```
Где http://localhost:5173/ - адрес нашего локального сервера. По нему можно кликнуть с зажатой ctrl, либо скопировать и вставить в браузер. Теперь любые сохранённые изменения будут сразу отображаться в браузере, без обновления страницы.
